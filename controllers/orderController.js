const Order = require("../models/Order");
const User = require("../models/User");
const Product = require("../models/Product");

// Viewing all orders from authenticated user
/*
	Logic:
	1. Get all orders from the orders collection that contain the user ID.
	2. Display the data.
*/
module.exports.viewOrdersFromUser = (sessionUser) => {
	return Order.find({"orderedBy.userId": sessionUser.id}).then(order => {
		if(sessionUser.isAdmin){
			return false;
		}
		else{
			return order;
		}
	}); // end return Order.find().then()
}; // end module.exports.viewOrdersFromUser

/*
	View all orders
	Logic:
	1. Get all orders from the database.
	2. Check if the user is admin.
	3. If the user is an admin, display all orders.
	4. If not, deny request.
*/
module.exports.viewAllOrders = (sessionData) => {
	return Order.find({}).then(result => {
		if(sessionData.isAdmin){
			return result;
		}
		else{
			return false;
		} // end else if(sessionData.isAdmin)
	}); // end return Order.find().then()
}; // end module.exports.viewAllOrders

/*
	Order Single Item(s) immediately.
	Business Logic:
	- Gather the product's data by product's id.
	- Check if the item is in stock/active or not.
	- If the item is out of stock/not active, deny request.
	- If active, create a new Order object to save it to database. put the selected product in the products array, then calculate the total amount by adding the price of each item.
	- Find the user document in the database using the user's ID that is logged in.
	- Put the newly-created order to the orders array in the users collection.
*/
module.exports.orderProduct = async (sessionUser, orderItems) => {
	let totalAmount = 0;
	let orderInfo = {
		products: [],
		totalAmount: 0,
		orderedBy: {}
	};
	// for(let i = 0; i <= orderItems.products.length - 1; i++){
	// 	totalAmount += orderItems.products[i].price;
	// } // end for
	// Check if item is in database
	let isProductFound = await Product.findById(orderItems.productId).then((product, err)  => {
		if(err){
			return false;
		} // end if(err)
		else{
			// Check if item is in stock
			if(product.isActive){
				// Assign to orderInfo
				orderInfo.products.push({
					productId: product.id,
					name: product.name,
					quantity: orderItems.quantity,
					price: product.price
				});
				// Compute total amount
				orderInfo.totalAmount = orderInfo.products[0].price * orderItems.quantity;
				return true;
			} // end if(product.isActive)
			else{
				return false;
			} // end else if(product.isActive)
		} // end else if(err)
	}); // end let isProductFound


	let isOrderCreated = await User.findById(sessionUser.id).then((user, err) => {
		if(err){
			return false;
		} // end if(err)
		else{
			if(!user.isAdmin){
				// IF USER IS NOT AN ADMIN
				// Get the session user and store to orderInfo.orderedBy
				orderInfo.orderedBy = {
					userId: user.id,
					email: user.email
				};
				let newOrder = new Order({
					products: orderInfo.products,
					orderedBy: orderInfo.orderedBy,
					totalAmount: orderInfo.totalAmount
				}); // end let newOrder
				return newOrder.save().then((saved, err) => {
					if(err){
						return false;
					}
					else{
						return true;
					}
				}); // end return newOrder.save().then()
			} // end if(!user.isAdmin)
			else{
				return false;
			}
		} // end else if(err)
	}); // end let isOrderCreated

	if(isOrderCreated){
		let isUserUpdated = await User.findById(sessionUser.id).then((user, err) => {
			if(err){
				return false;
			}
			else{
				return Order.find({userId: sessionUser.id}).then((order, err) => {
					if(err){
						return false;
					} // end if(err)
					else{
						// Save the LATEST order
						user.orders.push({
							orderId: order[order.length - 1].id,
							totalAmount: order[order.length - 1].totalAmount,
							products: order[order.length - 1].products
						}); // end user.orders.push()
						return user.save().then((saved, err) => {
							if(err){
								return false;
							} // end if(err)
							else{
								return true;
							} // end else if(err)
						}); // end return user.save().then()
					} // end else if(err)
				}); // end return Order.find().then()
			} // end else if(err)
		}); // end let isUserUpdated
		if(isUserUpdated){
			return true;
		} // end if(isUserUpdated)
		else{
			return false;
		} // end else if(isUserUpdated)
	} // end if(isOrderCreated)
	else{
		return false;
	} // end else if(isOrderCreated)

}; // end module.exports.orderProducts

/*
	Checkout items from cart
	Business Logic:
	- Find the user document in the database using the user's ID that is logged in.
	- Create a new Order object to save it to database. Put the products in the products array, then calculate the total amount by adding the price of each item. 
	- Save to database.
	- Put the newly-created order to the orders array in the users collection.
	- Empty the cart.
*/
module.exports.checkoutFromCart = async (sessionUser) => {
	let orderedBy = {};
	let isOrderCreated = await User.findById(sessionUser.id).then((user, err) => {
		if(err){
			return false;
		} // end if(err)
		else{
			if(user.cart.products.length > 0){
				orderedBy = {
					userId: user.id,
					email: user.email
				};
				let newOrder = new Order({
					products: user.cart.products,
					orderedBy: orderedBy,
					totalAmount: user.cart.totalAmount
				}); // end let newOrder
				return newOrder.save().then((saved, err) => {
					if(err){
						return false;
					} // end if(err)
					else{
						return true;
					} // end else if(err)
				}); // end return newOrder.save().then()
			} // end if(user.cart.products.length > 0)
			else{
				return false;
			} // end else if(user.cart.products.length > 0)
		} // end else if(err)
	}); // end let isOrderCreated

	if(isOrderCreated){
		let isUserUpdated = await User.findById(sessionUser.id).then((user, err) => {
			if(err){
				return false;
			} // end if(err)
			else{
				return Order.find({userId: user.id}).then((order, err) => {
					if(err){
						return false;
					} // end if(err)
					else{
						// Get latest order
						user.orders.push({
							orderId: order[order.length - 1].id,
							totalAmount: order[order.length - 1].totalAmount,
							products: order[order.length - 1].products
						}); // end user.orders.push()
						// clear user.cart
						user.cart = {
							products: [],
							totalAmount: 0
						};
						return user.save().then((saved, err) => {
							if(err){
								return false;
							}
							else{
								return true;
							} // end else if(err)
						}); // end return user.save().then()
					} // end else if(err)
				}); // return Order.findOne().then()
			} // end else if(err)
		}); // end let isUserUpdated
		if(isUserUpdated){
			return true;
		}
		else{
			return false;
		}
	} // end isOrderCreated
	else{
		return false;
	}

	// if(isOrderCreated && isUserUpdated){
	// 	return true;
	// }
	// else{
	// 	return false;
	// }
} // end module.exports.checkoutItems
